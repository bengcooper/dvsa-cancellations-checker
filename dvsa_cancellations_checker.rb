# frozen_string_literal: true

require 'watir'
require 'twilio-ruby'

# Finds DVSA cancellations
class DvsaCancellationsChecker
  def initialize(licence_number, booking_reference, sms_number,
                 twilio_account_sid, twilio_auth_token)
    @filename = 'last_check.txt'
    @sms_from = 'DVSAChecker'
    twilio = Twilio::REST::Client.new(twilio_account_sid, twilio_auth_token)
    browser = Watir::Browser.new :chrome
    browser.goto 'https://driverpracticaltest.direct.gov.uk/login'
    browser.text_field(id: 'driving-licence-number').set(licence_number)
    browser.text_field(id: 'application-reference-number').set(booking_reference)
    browser.button(id: 'booking-login').click
    browser.link(id: 'date-time-change').click
    browser.radio(id: 'test-choice-earliest').set
    browser.button(id: 'driving-licence-submit').click
    browser.td(class: 'BookingCalendar-date--bookable').click
    slot_picker = browser.li(class: ['SlotPicker-day', 'is-active'])
    date = slot_picker.p(class: 'SlotPicker-dayTitle').text
    times = slot_picker.strongs(class: 'SlotPicker-time').map(&:text)
    browser.close

    if File.size?(@filename)
      last_date = Date.parse(File.read(@filename))
      if Date.parse(date) != last_date
        twilio.messages.create from: @sms_from,
                               to: sms_number,
                               body: "Tests available on #{date} at #{times.join ' or '}"

      end
    end
    File.open(@filename, 'w') { |file| file.puts date }
  end
end
