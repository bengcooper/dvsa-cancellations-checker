# frozen_string_literal: true

require_relative 'dvsa_cancellations_checker'

DvsaCancellationsChecker.new ENV['LICENSE_NUMBER'],
                             ENV['BOOKING_REFERENCE'],
                             ENV['SMS_NUMBER'],
                             ENV['TWILIO_ACCOUNT_SID'],
                             ENV['TWILIO_AUTH_TOKEN']
